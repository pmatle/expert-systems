/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backward_chaining.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 06:40:24 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/25 12:00:28 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		validate(t_queries *queries)
{
	while (queries != NULL)
	{
		ft_putchar(queries->letter);
		if (queries->value == 1)
			ft_putendl(" is True");
		else
			ft_putendl(" is False");
		queries = queries->next;
	}
	return (1);
}

int		add_chars(t_chars **chars, char c)
{
	t_chars *new;

	if (!(new = (t_chars*)ft_memalloc(sizeof(*new))))
		return (0);
	new->c = c;
	new->next = (*chars);
	(*chars) = new;
	return (1);
}

int		backward_chaining(t_facts *facts, t_rules *rules, t_queries *queries)
{
	t_queries	*temp;

	temp = queries;
	while (temp != NULL)
	{
		temp->value = backwardchain(&facts, rules, &queries, temp->letter);
		temp = temp->next;
	}
	validate(queries);
	return (1);
}
