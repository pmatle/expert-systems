/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 14:48:26 by pmatle            #+#    #+#             */
/*   Updated: 2017/06/11 17:15:07 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t		x;
	size_t		y;
	char		*haystack;
	char		*niddle;

	x = 0;
	y = 0;
	haystack = (char*)big;
	niddle = (char*)little;
	if (ft_strlen(niddle) == 0)
		return (haystack);
	while (len > 0 && haystack[x] != '\0' && niddle[y] != '\0')
	{
		if (haystack[x] == niddle[y])
		{
			y++;
		}
		x++;
		len--;
	}
	if (ft_strlen(niddle) == y)
		return (ft_strstr(haystack, niddle));
	return (NULL);
}
