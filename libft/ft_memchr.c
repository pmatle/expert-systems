/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 10:30:29 by pmatle            #+#    #+#             */
/*   Updated: 2017/06/11 16:56:09 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*str;
	unsigned char	l;

	str = (unsigned char*)s;
	l = c;
	while (n > 0)
	{
		if (*str != l)
			str++;
		else
			return (str);
		n--;
	}
	return (NULL);
}
