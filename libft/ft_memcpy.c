/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 01:27:41 by pmatle            #+#    #+#             */
/*   Updated: 2017/06/11 16:50:22 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*dest;
	char	*source;
	int		x;

	dest = (char*)dst;
	source = (char*)src;
	x = 0;
	while (n > 0)
	{
		dest[x] = source[x];
		x++;
		n--;
	}
	return (dest);
}
