/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/08 08:08:20 by pmatle            #+#    #+#             */
/*   Updated: 2017/08/04 08:37:42 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	int	x;

	if (s1 == NULL || s2 == NULL)
		return (1);
	if (ft_strlen(s1) == 0 || ft_strlen(s2) == 0)
		return (1);
	x = ft_strncmp(s1, s2, n - 1);
	if (x == 0)
		return (1);
	else
		return (0);
}
