/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_facts.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 16:41:23 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/20 17:39:52 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

char	*search(t_infor *infor, char c)
{
	char	*facts;
	char	**line;

	while (infor != NULL)
	{
		if (infor->line[0] == c)
		{
			line = ft_strsplit(infor->line, '#');
			facts = ft_strdup(line[0] + 1);
			free_2d(line);
			return (facts);
		}
		infor = infor->next;
	}
	return (NULL);
}

int		add_facts(t_facts **head, int fact)
{
	t_facts		*new;

	if (!(new = (t_facts*)ft_memalloc(sizeof(*new))))
		return (0);
	new->fact = fact;
	new->next = (*head);
	(*head) = new;
	return (1);
}

t_facts	*get_facts(t_infor *infor)
{
	int		x;
	char	*str;
	t_facts	*facts;

	x = 0;
	facts = NULL;
	if (!(str = search(infor, '=')))
		return (NULL);
	str = ft_strtrim(str);
	while (str[x] != '\0')
	{
		if (str[x] != ' ')
			add_facts(&facts, str[x]);
		x++;
	}
	free(str);
	return (facts);
}
