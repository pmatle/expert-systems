/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_infor.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 15:44:28 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/18 15:46:44 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		free_infor(t_infor *infor)
{
	t_infor		*temp;

	while (infor != NULL)
	{
		temp = infor;
		infor = infor->next;
		free(temp);
	}
	return (1);
}
