/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_errors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 10:00:26 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/22 15:32:28 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		check_errors(t_facts *facts, t_rules *rules, t_queries *queries)
{
	if (!rules || !queries)
	{
		ft_putendl_fd("Error: File specified has incorrect format", 2);
		return (0);
	}
	if (!check_rules_errors(rules))
	{
		ft_putendl_fd("Error: There is an error in the rules given", 2);
		return (0);
	}
	if (!check_facts_errors(facts))
	{
		ft_putendl_fd("Error: There is an error in the facts statement", 2);
		return (0);
	}
	if (!check_queries_errors(queries))
	{
		ft_putendl_fd("Error: There is an error in the query statement", 2);
		return (0);
	}
	return (1);
}
