/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_rule.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 06:57:58 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/22 11:12:56 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int			add_result(t_results **head, t_rules *rule)
{
	t_results	*new;

	if (!(new = (t_results*)ft_memalloc(sizeof(*new))))
		return (0);
	new->rule = rule;
	new->next = (*head);
	(*head) = new;
	return (1);
}

t_results	*search_rule(t_rules *rules, char c)
{
	t_rules		*temp;
	t_results	*results;

	temp = rules;
	results = NULL;
	while (temp != NULL)
	{
		if (ft_strchr(temp->conclusion, c))
			add_result(&results, temp);
		temp = temp->next;
	}
	return (results);
}
