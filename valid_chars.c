/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_chars.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 08:21:39 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/19 17:08:54 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		isspace(char c)
{
	if (c == 32 || (c >= 9 && c <= 13))
		return (1);
	return (0);
}

int		valid_chars(char c)
{
	if (ft_isalpha(c) && ft_tolower(c) == c)
		return (0);
	if (ft_isalpha(c) || c == '!' || c == '+' || c == '|' || c == '^' ||
			isspace(c) || c == '(' || c == ')')
		return (1);
	return (0);
}
