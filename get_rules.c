/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rules.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 17:21:51 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/20 16:08:52 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int			if_rule(char *line)
{
	int		x;

	x = 0;
	if (ft_strstr(line, "=>"))
		return (1);
	return (0);
}

int			add_rule(t_rules **head, char *rule, char *conclusion)
{
	t_rules		*new;

	if (!(new = (t_rules*)ft_memalloc(sizeof(*new))))
		return (0);
	new->statement = rule;
	new->conclusion = conclusion;
	new->next = (*head);
	(*head) = new;
	return (1);
}

char		*get_str(char *str, char c)
{
	int		x;
	int		len;
	char	*s1;

	x = 0;
	if (c == 's')
	{
		len = check_char(str, '=', '<');
		s1 = ft_strsub(str, 0, len);
	}
	else
	{
		while (str[x] == '<' || str[x] == '=' || str[x] == '>')
			x++;
		s1 = ft_strdup(str);
	}
	return (s1);
}

int			separate(char *line, char **conclusion, char **statement)
{
	int		x;
	char	**array;
	char	**rule;
	char	*temp;

	x = 0;
	if (!ft_strstr(line, "=>"))
		return (0);
	array = ft_strsplit(line, '#');
	temp = ft_strdup(array[0]);
	temp = ft_strtrim(temp);
	rule = ft_strsplit(temp, '>');
	*statement = get_str(rule[0], 's');
	*conclusion = get_str(rule[1] + 1, 'c');
	*statement = ft_strtrim(*statement);
	*conclusion = ft_strtrim(*conclusion);
	free_2d(rule);
	free_2d(array);
	free(temp);
	return (1);
}

t_rules		*get_rules(t_infor *infor)
{
	char	*statement;
	char	*conclusion;
	t_rules	*rules;

	rules = NULL;
	while (infor != NULL)
	{
		if (if_rule(infor->line))
		{
			if (!separate(infor->line, &conclusion, &statement))
				return (0);
			add_rule(&rules, statement, conclusion);
		}
		infor = infor->next;
	}
	return (rules);
}
