/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_parenthesis.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 08:59:44 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/19 08:09:26 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		check_matching(char *str, char *stack, int *sp, int *x)
{
	if (str[*x] == ')' && stack[*sp] == '(')
		(*sp)--;
	else if (str[*x] == '}' && stack[*sp] == '{')
		(*sp)--;
	else if (str[*x] == ']' && stack[*sp] == '[')
		(*sp)--;
	else
	{
		free(stack);
		return (0);
	}
	return (1);
}

int		check_paranthesis(char *str)
{
	char	*stack;
	int		sp;
	int		x;

	if (!(stack = (char*)ft_memalloc(sizeof(*stack) * 1024)))
		return (0);
	sp = -1;
	x = 0;
	while (str[x] != '\0')
	{
		if (str[x] == '(' || str[x] == '{' || str[x] == '[')
		{
			sp++;
			stack[sp] = str[x];
		}
		else if (str[x] == ')' || str[x] == '}' || str[x] == ']')
		{
			if (check_matching(str, stack, &sp, &x) == 0)
				return (0);
		}
		x++;
	}
	free(stack);
	return (sp == -1) ? 1 : 0;
}
