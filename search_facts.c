/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_facts.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 12:29:47 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/22 11:13:21 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		search_fact(t_facts *facts, char c)
{
	while (facts != NULL)
	{
		if (facts->fact == c)
			return (1);
		facts = facts->next;
	}
	return (0);
}
