/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expert_system.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 11:33:52 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/24 07:51:45 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int			add_infor(t_infor **head, char *line)
{
	t_infor		*new;

	if (!(new = (t_infor*)malloc(sizeof(*new))))
		return (0);
	new->line = line;
	new->next = (*head);
	(*head) = new;
	return (1);
}

t_infor		*get_infor(char *str)
{
	t_infor		*infor;
	int			fd;
	char		*line;

	infor = NULL;
	if (access(str, F_OK) == -1)
	{
		ft_putendl_fd("Error: The file specified does not exit", 2);
		exit(0);
	}
	fd = open(str, O_RDONLY);
	while (get_next_line(fd, &line))
	{
		line = ft_strtrim(line);
		if (line[0] != '#' && ft_strlen(line) != 0)
		{
			add_infor(&infor, line);
		}
	}
	return (infor);
}

int			main(int argc, char **argv)
{
	t_infor		*infor;

	infor = NULL;
	if (argc == 2)
	{
		infor = get_infor(argv[1]);
		start_engine(infor);
	}
	return (0);
}
