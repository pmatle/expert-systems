/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expert_system.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 15:10:38 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/25 12:33:19 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXPERT_SYSTEM_H
# define EXPERT_SYSTEM_H

# include <fcntl.h>
# include "get_next_line/get_next_line.h"
# include "libft/includes/libft.h"
# include <stdio.h>

typedef struct	s_infor
{
	char			*line;
	struct s_infor	*next;
}				t_infor;

typedef struct	s_facts
{
	char			fact;
	struct s_facts	*next;
}				t_facts;

typedef struct	s_rules
{
	char			*statement;
	char			*conclusion;
	struct s_rules	*next;
}				t_rules;

typedef struct	s_queries
{
	char				letter;
	int					solved;
	int					value;
	struct s_queries	*next;
}				t_queries;

typedef struct	s_goal
{
	int				val;
	struct s_goal	*next;
}				t_goal;

typedef struct	s_stack
{
	char			c;
	int				solved;
	int				value;
	struct s_stack	*next;
}				t_stack;

typedef struct	s_results
{
	t_rules				*rule;
	struct s_results	*next;
}				t_results;

typedef struct	s_vals
{
	int				val;
	struct s_vals	*next;
}				t_vals;

typedef struct	s_chars
{
	char			c;
	struct s_chars	*next;
}				t_chars;

int				start_engine(t_infor *infor);
char			*search(t_infor *infor, char c);
t_facts			*get_facts(t_infor *infor);
int				check_char(char *str, char a, char b);
t_rules			*get_rules(t_infor *infor);
t_queries		*get_queries(t_infor *infor);
int				check_paranthesis(char *str);
int				push(t_stack **head, char operator);
int				pop(t_stack **head);
int				count(t_stack *stack);
char			*infix_to_postfix(char *expression);
char			*remove_extra_spaces(char *str);
int				valid_chars(char c);
int				backward_chaining(t_facts *f, t_rules *r, t_queries *q);
t_results		*search_rule(t_rules *rules, char c);
int				search_fact(t_facts *facts, char c);
int				add_facts(t_facts **head, int fact);
int				add_queries(t_queries **head, char query);
int				backwardchain(t_facts **f, t_rules *r, t_queries **q, char c);
t_queries		*get_statement_letter(char *str);
int				rpn_calculator(t_facts *facts, char *str);

int				check_errors(t_facts *f, t_rules *r, t_queries *q);
int				check_rules_errors(t_rules *rules);
int				check_queries_errors(t_queries *queries);
int				check_facts_errors(t_facts *facts);

int				free_2d(char **array);
int				free_data(t_facts *f, t_rules *r, t_queries *q);
int				free_infor(t_infor *infor);
int				free_facts(t_facts *facts);
int				free_rules(t_rules *rules);
int				free_queries(t_queries *queries);

#endif
