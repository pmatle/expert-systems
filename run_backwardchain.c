/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_backwardchain.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 11:49:31 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/29 11:41:36 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		print_steps(t_results *results, char c)
{
	ft_putstr("Goal : ");
	ft_putchar(c);
	ft_putchar('\n');
	ft_putstr("Rule Found : ");
	ft_putstr(results->rule->statement);
	ft_putstr(" => ");
	ft_putendl(results->rule->conclusion);
	return (1);
}

int		add(t_vals **head, int x)
{
	t_vals		*new;

	if (!(new = (t_vals*)ft_memalloc(sizeof(*new))))
		return (0);
	new->val = x;
	new->next = (*head);
	(*head) = new;
	return (1);
}

int		check(t_vals *vals)
{
	int		x;

	x = vals->val;
	while (vals != NULL)
	{
		if (x != vals->val)
			return (0);
		vals = vals->next;
	}
	return (1);
}

int		free_vals(t_vals *head)
{
	t_vals *temp;

	while (head != NULL)
	{
		temp = head;
		head = head->next;
		free(temp);
	}
	return (1);
}

int		count_rules(t_rules *rules)
{
	int		x;

	x = 0;
	while (rules != NULL)
	{
		x++;
		rules = rules->next;
	}
	return (x);
}

int		add_conclusions(t_facts **facts, t_rules *rules)
{
	t_queries	*temp;

	temp = get_statement_letter(rules->conclusion);
	while (temp != NULL)
	{
		add_facts(&(*facts), temp->letter);
		temp = temp->next;
	}
	return (1);
}

int		backwardchain(t_facts **f, t_rules *r, t_queries **q, char c)
{
	t_results	*results;
	t_queries	*temp;
	t_vals		*vals;
	int			flag;
	static int	iterate = 0;

	flag = 0;
	vals = NULL;
	if (search_fact(*f, c))
		return (1);
	if (!(results = search_rule(r, c)))
		return (0);
	print_steps(results, c);
	iterate++;
	while (results != NULL)
	{

		temp = get_statement_letter(results->rule->statement);
		if ((count_rules(r) * 10) < iterate)
		{
			while (*q != NULL)
			{
				ft_putchar((*q)->letter);
				if (search_fact(*f, (*q)->letter))
					ft_putendl(" is True");
				else
					ft_putendl(" is Undetermined");
				(*q) = (*q)->next;
			}
			free_data(*f, r, *q);
			free_vals(vals);
			exit(0);
		}
		while (temp != NULL)
		{
			backwardchain(f, r, q, temp->letter);
			temp = temp->next;
		}
		if (rpn_calculator(*f, results->rule->statement))
		{
			add_conclusions(&(*f), results->rule);
			add(&vals, 1);
			flag = 1;
		}
		else
			add(&vals, 0);
		free_queries(temp);
		results = results->next;
	}
	if (!check(vals))
	{
		free_data(*f, r, *q);
		free_vals(vals);
		ft_putendl_fd("Error: There is a contradiction in the facts", 2);
		exit(0);
	}
	return ((flag) ? 1 : 0);
}
