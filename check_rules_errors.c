/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_rules_errors.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 07:46:32 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/19 17:10:41 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

char	*remove_extra_spaces(char *str)
{
	int		x;
	int		y;
	int		len;
	char	*new;

	x = 0;
	y = 0;
	len = ft_strlen(str);
	new = (char*)ft_memalloc(sizeof(*new) * len + 1);
	while (str[x] != '\0')
	{
		if (str[x] == ' ')
		{
			new[y] = str[x];
			y++;
			while (str[x] == ' ')
				x++;
		}
		new[y] = str[x];
		y++;
		x++;
	}
	new[y] = '\0';
	return (new);
}

int		is_valid_rules(char *str)
{
	int		x;

	x = 0;
	while (str[x] != '\0')
	{
		if (!valid_chars(str[x]))
			return (0);
		x++;
	}
	if (!check_paranthesis(str))
		return (0);
	return (1);
}

int		check_rules_errors(t_rules *rules)
{
	char	*statement;
	char	*conclusion;

	while (rules != NULL)
	{
		statement = remove_extra_spaces(rules->statement);
		conclusion = remove_extra_spaces(rules->conclusion);
		if (!is_valid_rules(statement))
			return (0);
		if (!is_valid_rules(conclusion))
			return (0);
		rules = rules->next;
	}
	free(statement);
	free(conclusion);
	return (1);
}
