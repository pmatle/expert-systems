/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_queries.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 12:47:41 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/22 11:13:50 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		free_queries(t_queries *queries)
{
	t_queries	*temp;

	while (queries != NULL)
	{
		temp = queries;
		queries = queries->next;
		free(temp);
	}
	return (1);
}
