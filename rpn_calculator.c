/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpn_calculator.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 08:56:25 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/25 18:09:52 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		add_to_stack(t_goal **head, int val)
{
	t_goal		*new;

	if (!(new = (t_goal *)ft_memalloc(sizeof(*new))))
		return (0);
	new->val = val;
	new->next = (*head);
	(*head) = new;
	return (1);
}

int		remove_top(t_goal **head)
{
	int		x;
	t_goal	*temp;

	temp = (*head);
	(*head) = (*head)->next;
	x = temp->val;
	free(temp);
	return (x);
}

int		evaluate(t_goal **head, char c)
{
	int		x1;
	int		x2;

	if (c == '+' || c == '^' || c == '|')
	{
		x2 = remove_top(&(*head));
		x1 = remove_top(&(*head));
		if (c == '+')
			add_to_stack(&(*head), x1 & x2);
		else if (c == '^')
			add_to_stack(&(*head), x1 ^ x2);
		else if (c == '|')
			add_to_stack(&(*head), x1 | x2);
	}
	else
	{
		x1 = remove_top(&(*head));
		if (c == '!')
			add_to_stack(&(*head), !x1);
	}
	return (1);
}

int		rpn_calculator(t_facts *facts, char *str)
{
	t_goal		*stack;
	int			x;

	x = 0;
	stack = NULL;
	str = infix_to_postfix(str);
	while (str[x] != '\0')
	{
		if (ft_isalpha(str[x]))
		{
			add_to_stack(&stack, search_fact(facts, str[x]));
		}
		else if (valid_chars(str[x]))
		{
			evaluate(&stack, str[x]);
		}
		x++;
	}
	return (remove_top(&(stack)));
}
