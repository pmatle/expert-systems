/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_data.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 11:24:31 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/22 13:32:09 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		free_data(t_facts *facts, t_rules *rules, t_queries *q)
{
	if (rules != NULL)
		free_rules(rules);
	if (q != NULL)
		free_queries(q);
	if (facts != NULL)
		free_facts(facts);
	return (1);
}
