/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 07:53:20 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/25 08:21:08 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		push(t_stack **head, char operator)
{
	t_stack *new;

	if (!(new = (t_stack*)ft_memalloc(sizeof(*new))))
		return (0);
	new->c = operator;
	new->next = (*head);
	(*head) = new;
	return (1);
}

int		pop(t_stack **head)
{
	t_stack 	*temp;
	int			c;

	temp = (*head);
	(*head) = (*head)->next;
	c = temp->c;
	free(temp);
	return (c);
}

int		count(t_stack *stack)
{
	int		x;

	x = 0;
	while (stack != NULL)
	{
		x++;
		stack = stack->next;
	}
	return (x);
}
