/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_engine.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 16:16:50 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/25 13:23:23 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		start_engine(t_infor *infor)
{
	t_facts		*facts;
	t_rules		*rules;
	t_queries	*queries;

	facts = get_facts(infor);
	rules = get_rules(infor);
	queries = get_queries(infor);
	if (!check_errors(facts, rules, queries))
	{
		free(infor);
		free_data(facts, rules, queries);
		exit(0);
	}
	free_infor(infor);
	backward_chaining(facts, rules, queries);
	free_data(facts, rules, queries);
	return (1);
}
