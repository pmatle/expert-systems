# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/09/07 10:55:12 by pmatle            #+#    #+#              #
#    Updated: 2017/10/29 15:10:21 by pmatle           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = expert_system

SRC = expert_system.c start_engine.c get_facts.c get_rules.c get_queries.c \
	  check_errors.c check_rules_errors.c check_queries_errors.c \
	  check_facts_errors.c valid_chars.c free_2d.c free_data.c free_facts.c \
	  free_queries.c free_rules.c free_infor.c backward_chaining.c \
	  search_rule.c check_parenthesis.c infix_to_postfix.c check_char.c \
	  rpn_calculator.c search_facts.c get_statement_letter.c \
	  run_backwardchain.c stack.c Get_next_line/get_next_line.c \

OBJ = expert_system.o start_engine.o get_facts.o get_rules.o get_queries.o \
	  check_errors.o check_rules_errors.o check_queries_errors.o \
	  check_facts_errors.o valid_chars.o free_2d.o free_data.o free_facts.o \
	  free_queries.o free_rules.o free_infor.o backward_chaining.o \
	  search_rule.o check_parenthesis.o infix_to_postfix.o check_char.o \
	  rpn_calculator.o search_facts.o get_statement_letter.o \
	  run_backwardchain.o stack.o Get_next_line/get_next_line.o \

FLAGS = -Wall -Wextra -Werror 

LIB = -L libft/ -lft

all: $(NAME)

$(NAME):
	make -C libft/ all
	gcc -o $(NAME) $(SRC) $(FLAGS) $(LIB) 
clean:
	make -C libft/ clean
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm -f $(NAME)
	make -C libft/ fclean

re:	fclean all
