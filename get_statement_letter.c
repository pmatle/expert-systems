/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_statement_letter.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 12:58:18 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/22 13:41:14 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

t_queries	*get_statement_letter(char *str)
{
	t_queries	*query;
	int			x;

	x = 0;
	query = NULL;
	while (str[x] != '\0')
	{
		if (ft_isalpha(str[x]))
			add_queries(&query, str[x]);
		x++;
	}
	return (query);
}
