/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_rules.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 14:23:03 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/18 14:46:32 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		free_rules(t_rules *rules)
{
	t_rules		*temp;

	while (rules != NULL)
	{
		temp = rules;
		rules = rules->next;
		free(temp);
	}
	return (1);
}
