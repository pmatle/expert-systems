/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_queries.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 08:12:56 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/25 13:22:33 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int			add_queries(t_queries **head, char query)
{
	t_queries	*new;

	if (!(new = (t_queries*)ft_memalloc(sizeof(*new))))
		return (0);
	new->letter = query;
	new->solved = 0;
	new->next = (*head);
	(*head) = new;
	return (1);
}

t_queries	*get_queries(t_infor *infor)
{
	int			x;
	char		*str;
	t_queries	*queries;

	x = 0;
	queries = NULL;
	if (!(str = search(infor, '?')))
		return (0);
	str = ft_strtrim(str);
	while (str[x] != '\0')
	{
		if (str[x] != ' ')
			add_queries(&queries, str[x]);
		x++;
	}
	free(str);
	return (queries);
}
