/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_facts_errors.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 15:58:52 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/20 07:52:38 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		check_facts_errors(t_facts *facts)
{
	char	c;

	while (facts != NULL)
	{
		c = facts->fact;
		if (!(c >= 65 && c <= 90))
			return (0);
		facts = facts->next;
	}
	return (1);
}
