/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_facts.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 12:44:40 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/18 12:47:21 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		free_facts(t_facts *facts)
{
	t_facts *temp;

	while (facts != NULL)
	{
		temp = facts;
		facts = facts->next;
		free(temp);
	}
	return (1);
}
