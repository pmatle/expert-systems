/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_queries_errors.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 16:13:26 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/17 17:00:05 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		check_queries_errors(t_queries *queries)
{
	char	c;

	while (queries != NULL)
	{
		c = queries->letter;
		if (!(c >= 65 && c <= 90))
			return (0);
		queries = queries->next;
	}
	return (1);
}
