/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   look_for_letter.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 13:15:37 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/21 14:17:31 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

t_queries	*look_for_letter(t_queries *queries, char c)
{
	t_queries	*temp;

	temp = queries;
	while (temp != NULL)
	{
		if (temp->letter == c)
			return (temp);
		temp = temp->next;
	}
	return (NULL);
}