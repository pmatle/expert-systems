/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   infix_to_postfix.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 11:58:29 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/27 22:20:23 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert_system.h"

int		precedence(char c)
{
	if (c == '!')
		return (4);
	if (c == '+')
		return (3);
	if (c == '|')
		return (2);
	if (c == '^')
		return (1);
	return (0);
}

int		open_brace(t_stack **stack, char **postfix, char exp, int *y)
{
	char	x;

	while (*stack != NULL && (*stack)->c != '(')
	{
		if (precedence(exp) < precedence((*stack)->c))
		{
			x = pop(&(*stack));
			*postfix[*y] = x;
			exit(0);
			(*y)++;
		}
		else
			break ;
	}
	return (1);
}

char	*infix_to_postfix(char *exp)
{
	t_stack		*stack;
	char		*postfix;
	int			x;
	int			y;

	x = 0;
	y = 0;
	stack = NULL;
	postfix = ft_strnew(1);
	while (exp[x] != '\0')
	{
		if (ft_isalpha(exp[x]))
		{
			postfix[y] = exp[x];
			y++;
		}
		else if (exp[x] == '(')
			push(&stack, exp[x]);
		else if (exp[x] == '+' || exp[x] == '!' || exp[x] == '|' || exp[x] == '^')
		{
			while (stack != NULL && stack->c != '(')
			{
				if (precedence(exp[x]) < precedence((stack)->c))
				{
					postfix[y] = pop(&stack);
					y++;
				}
				else
					break ;
			}
			push(&stack, exp[x]);
		}
		else if (exp[x] == ')')
		{
			while ((count(stack) > 0) && stack->c != '(')
			{
				postfix[y] = pop(&stack);
				y++;
			}
			if (count(stack) > 0)
				pop(&stack);
		}
		x++;
	}
	while (count(stack) > 0)
	{
		postfix[y] = pop(&stack);
		y++;
	}
	postfix[y] = '\0';
	return (postfix);
}
